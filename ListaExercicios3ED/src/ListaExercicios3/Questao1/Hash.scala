package ListaExercicios3.Questao1

import scala.collection.mutable.ListBuffer

class HashTable {
    var size:Int = 0
    var table:Array[ListBuffer[Int]] = null
    
    //Nome:CriarTabela
    //Descricao: Cria Tabela Hash
    def createTable(n:Int) = {
        size = n/2
        table = new Array[ListBuffer[Int]](size)
    }
    
    //Nome:FuncaoHash
    //Descricao: Esse metodo retorna a posicao em que o elemento deve ser inserido na tabela
    def HashFunction(value:Int):Int ={
        value%size // A funcao hash retorna o resto da divisao do valor pelo tamanho da tabela
    }
    
    //Nome:Adicionar
    //Descricao: Esse metodo adiciona um valor a tabela
    //Observacao: Como cada elemento da tabela hash e uma lista, entao havendo colisao 
    //o novo valor e apenas adicionado ao final da lista referente a posicao da tabela 
    //que deve ser adicionado segunda a funcao hash
    def add(value:Int)={
        if( table(HashFunction(value)) == null )
            table(HashFunction(value)) = new ListBuffer[Int]()
        table(HashFunction(value)) += value
    } 
    
    
    //Nome:busca
    //Descricao: Esse metodo retorna a posicao de um valor na tabela hash
    def search(value:Int): Option[(Int,Int)] = {
        val hashIdx = HashFunction(value)
        table(hashIdx).indexWhere( v => v == value) match {
		      case x if x > -1 => Some((hashIdx, x))
		      case qqCoisa => None		
      	}
    }
    
    //Nome:Remover
    //Descricao: Esse metodo remove um valor da tabela
    def delete(value:Int) = {
        search(value) match {
		      case Some(tuple) => table(tuple._1).remove(tuple._2)
		      case None =>
	      }
        
         
    }
    
    //Nome:Liberar
    //Descricao: Libera tabela
    def free()={
        table = null
    }
}