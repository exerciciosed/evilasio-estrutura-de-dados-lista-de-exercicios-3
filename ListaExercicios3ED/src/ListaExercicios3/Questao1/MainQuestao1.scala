package ListaExercicios3.Questao1

object MainQuestao1 {
    def main(args: Array[String]) {
        var hash:HashTable = new HashTable
        
        //cria tabela
        hash.createTable(10)
        println("Criada uma hash de tamanho 10")
        
        
        //Insere valores
        hash.add(2)
        hash.add(12)
        hash.add(0)
        println("Adicionado os valores 2, 12 e 0")
        
        //Buscar um elemento
        var pos = hash.search(2)
        println("O valor 2 esta na posicao: " + pos)
        pos = hash.search(12)
        println("O valor 12 esta na posicao: " + pos)
        pos = hash.search(0)
        println("O valor 0 esta na posicao: " + pos)
        
        //Remove elemento
        hash.delete(12)
        println("O valor 12 foi removido")
        
        //Libera hash
        hash.free()
        println("libera tabela")
    }
}
